variable "instance_key_path" {
  default = "./ssh/snapshotter"
}

variable "instance_key_name" {
  default = "snapshotter-key-pair"
}

variable "software_package_download_url" {
  default = "https://packages.chef.io/files/stable/chef-server/12.17.33/ubuntu/16.04/chef-server-core_12.17.33-1_amd64.deb"
}

variable "snapshot_ami_name_prefix" {
  default = "chef-server"
}

variable "snapshot_stable_default_tag_value" {
  type = "string"
  default = "true"
}

variable "region" {
  default = "eu-west-1"
}

# auto-populated if blank
variable "vpc_id" {
  default = ""
}

variable "subnet_id" {
  default = ""
}

variable "accept_licence_terms" {
  description = "provide value \"true\" if you accept the terms of the license agreement of the being installed software. Type anything to reject them (installation may fail). "
  type = "string"
}