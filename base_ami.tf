data "aws_ami" "base_os" {
  most_recent = true

  filter {
    name = "name"
    values = ["ubuntu/images/*/ubuntu-xenial-16.04-*-server-*"]
  }

  filter {
    name = "architecture"
    values = ["x86_64"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

