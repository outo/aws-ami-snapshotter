#!/usr/bin/env bash

if [ ! -f ${KEY_PATH} ]; then
    ssh-keygen -t rsa -b 2048 -f ${KEY_PATH} -N ''
    chmod 600 ${KEY_PATH}
fi

