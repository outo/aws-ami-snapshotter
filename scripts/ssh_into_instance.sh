#!/usr/bin/env bash

ssh \
    -i $(terraform output | awk '/private_key_path/ {print $3}') \
    ubuntu@$(terraform output | awk '/instance_public_ip/ {print $3}')
