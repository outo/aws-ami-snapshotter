#!/usr/bin/env bash

SNAPSHOT_AMI_ID=$(aws ec2 create-image --instance-id ${INSTANCE_ID} --name "${SNAPSHOT_NAME}" --query 'ImageId' --output text | tr -d '\r\n')

until $(aws ec2 wait image-available --image-ids ${SNAPSHOT_AMI_ID})
do
    echo "Waiting for an AMI to be created"
    sleep 10
done

aws ec2 create-tags --resources ${SNAPSHOT_AMI_ID} --tags "Key=stable,Value=${STABLE}" "Key=software_package,Value=${SOFTWARE_PACKAGE}"

until $(aws ec2 wait image-available --image-ids ${SNAPSHOT_AMI_ID} --filters "Name=tag:stable,Values=${STABLE}" "Name=tag:software_package,Values=${SOFTWARE_PACKAGE}")
do
    echo "Waiting for an AMI to be tagged"
    sleep 10
done

