resource "aws_default_vpc" "default_vpc" {}

locals {
  vpc_id = "${var.vpc_id != "" ? var.vpc_id : aws_default_vpc.default_vpc.id}"
}

data "aws_subnet_ids" "available" {
  vpc_id = "${local.vpc_id}"
}

locals {
  subnet_id = "${var.subnet_id != "" ? var.subnet_id : element(data.aws_subnet_ids.available.ids, 0)}"
}