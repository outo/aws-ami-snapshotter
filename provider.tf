provider "aws" {
  region = "${var.region}"
  version = "~> 1.36.0"
}

provider "local" {
  version = "~>1.1.0"
}

provider "random" {
  version = "~>2.0.0"
}