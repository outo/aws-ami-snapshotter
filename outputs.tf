output "region" {
  value = "${var.region}"
}

output "private_key_path" {
  value = "${data.local_file.priv_key.filename}"
}

output "instance_public_ip" {
  value = "${aws_instance.instance.public_ip}"
}

output "my_snapshots_most_recent_first" {
  value = "${data.aws_ami_ids.my_snapshots.ids}"
}

output "my_available_stable_snapshots_most_recent_first" {
  value = "${data.aws_ami_ids.my_available_stable_snapshots.ids}"
}