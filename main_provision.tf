
resource "aws_security_group" "instance" {
  name = "allow_ssh_in_and_all_out"
  description = "Allow inbound SSH and all outbound"
  vpc_id = "${local.vpc_id}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

//  open up to a web browser as one way of verifying that your install script does good job,
//  before tagging an AMI as stable
//  ingress {
//    from_port = 443
//    to_port = 443
//    protocol = "tcp"
//    cidr_blocks = [
//      "0.0.0.0/0"]
//  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}

resource "aws_instance" "instance" {
  ami = "${data.aws_ami.base_os.image_id}"

//  fairly large instance, you may want smaller
  instance_type = "t2.xlarge"

  key_name = "${var.instance_key_name}"
  vpc_security_group_ids = ["${aws_security_group.instance.id}"]
  subnet_id = "${local.subnet_id}"

  tags {
    Name = "ami-snapshotter-${var.snapshot_ami_name_prefix}"
  }

}

