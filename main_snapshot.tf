resource "random_string" "suffix" {
  length = 10
  special = false
}

locals {
  snapshot_ami_name = "${var.snapshot_ami_name_prefix}-${random_string.suffix.result}"
}

# this hack will allow for the stack to be destroyed leaving the ami copy intact
resource "null_resource" "take_snapshot" {
  depends_on = ["null_resource.install"]
  provisioner "local-exec" {
    command = "bash ./scripts/snapshot.sh"

    environment {
      INSTANCE_ID = "${aws_instance.instance.id}"
      SNAPSHOT_NAME = "${local.snapshot_ami_name}"
      STABLE = "${var.snapshot_stable_default_tag_value}"
      SOFTWARE_PACKAGE = "${var.software_package_download_url}"
    }
  }
}

data "aws_caller_identity" "current" {}

data "aws_ami_ids" "my_snapshots" {
  depends_on = ["null_resource.take_snapshot"]
  owners = [
    "${data.aws_caller_identity.current.account_id}"]

  filter {
    name = "name"
    values = [
      "${var.snapshot_ami_name_prefix}-*"]
  }
}

data "aws_ami_ids" "my_available_stable_snapshots" {
  depends_on = ["null_resource.take_snapshot"]
  owners = [
    "${data.aws_caller_identity.current.account_id}"]

  filter {
    name = "tag:stable"
    values = [
      "true"]
  }

  filter {
    name = "state"
    values = [
      "available"]
  }

  filter {
    name = "name"
    values = [
      "${var.snapshot_ami_name_prefix}-*"]
  }
}
