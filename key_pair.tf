resource "null_resource" "create_key" {
  provisioner "local-exec" {
    command = "bash ./scripts/create_key_if_missing.sh"

    environment {
      KEY_PATH = "${var.instance_key_path}"
    }
  }
}

data "local_file" "pub_key" {
  depends_on = ["null_resource.create_key"]
  filename = "${var.instance_key_path}.pub"
}

data "local_file" "priv_key" {
  depends_on = ["null_resource.create_key"]
  filename = "${var.instance_key_path}"
}

resource "aws_key_pair" "instance_key_pair" {
  key_name = "${var.instance_key_name}"
  public_key = "${data.local_file.pub_key.content}"
}
