#!/usr/bin/env bash

echo "${package_download_url}" > package.txt

if [ ${accept_license} = "true" ]; then
    touch license-accepted.txt

    wget ${package_download_url} -O "package.deb"

    sudo dpkg -i ./package.deb
    sudo chef-server-ctl reconfigure

    sudo chef-server-ctl install chef-manage
    sudo chef-server-ctl reconfigure
    sudo chef-manage-ctl reconfigure --accept-license

    sudo chef-server-ctl install opscode-push-jobs-server
    sudo chef-server-ctl reconfigure
    sudo opscode-push-jobs-server-ctl reconfigure

    # legacy extension
    sudo chef-server-ctl install opscode-reporting
    sudo chef-server-ctl reconfigure
    sudo opscode-reporting-ctl reconfigure --accept-license

    echo "installed"
else
    touch license-rejected.txt
fi

rm package.deb
