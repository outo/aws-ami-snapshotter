
data "template_file" "install" {
  template = "${file("${path.root}/templates/install.sh")}"

  vars {
    package_download_url = "${var.software_package_download_url}"
    accept_license = "${var.accept_licence_terms}"
  }
}

resource "null_resource" "install" {
  provisioner "remote-exec" {
    inline = [
      "echo \"${data.template_file.install.rendered}\" > /tmp/install.sh",
      "sudo chmod +x /tmp/install.sh",
      "/tmp/install.sh"
    ]
  }

  connection {
    host = "${aws_instance.instance.public_ip}"
    port = 22
    type = "ssh"
    user = "ubuntu"
    private_key = "${data.local_file.priv_key.content}"
  }

}
